<?php

require_once 'Data/DataLayer.php';
require_once 'Util/Mailer.php';
require_once 'Util/Email/Email.php';
require_once 'Util/Email/MailerException.php';
require_once 'Util/Email/MailTypes/RemindEmail.php';
require_once 'Util/Email/MailTypes/NewCustomerEmail.php';