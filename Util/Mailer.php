<?php

class Mailer
{
    /**
     * Customers array
     *
     * @var
     */
    protected $customers;

    /**
     * Contains Email array
     *
     * @var
     */
    protected $mailers;

    /**
     * Mode of output. Setups output function.
     *
     * @var string
     */
    protected $mode;

    /**
     * Contains configured mail list for customers
     *
     * @var array
     */
    protected $mailList = [];

    /**
     * Mailer constructor.
     * @param string $mode
     */
    public function __construct($mode = 'production')
    {
        try {
            if (method_exists($this, $mode . "Output"))
                $this->mode = $mode;
            else
                throw new MailerException("No such output method \"{$mode}\" specified");
        } catch (MailerException $exception) {
            die($exception);
        }
    }

    /**
     * @param $name
     * @param $args
     * @return mixed|null
     */
    public function __call($name, $args)
    {
        return isset($this->$name) ? call_user_func_array($this->$name, $args) : null;
    }

    /**
     * Add customers list, optionally add customers orders
     *
     * @param array $customers
     * @param array $orders
     */
    public function addCustomers(array $customers = [], array $orders = [])
    {
        $this->customers = $customers;

        if (!empty($orders))
            $this->addOrders($orders);
    }

    /**
     * Attach Orders to Customers by email
     *
     * @param array $orders
     */
    public function addOrders(array $orders = [])
    {
        foreach ($this->customers as &$customer) {
            foreach ($orders as $order) {
                if (isset($order->customerEmail) && isset($customer->email) && $order->customerEmail === $customer->email)
                    $customer->orders[] = $order;
            }
        }
    }

    /**
     * Add single Email to mailers list
     *
     * @param Email|null $mail
     */
    public function addMailer($mail = null)
    {
        if ($mail instanceof Email)
            $this->mailers[] = $mail;
    }

    /**
     * Add mailers array
     *
     * @param array $mails
     */
    public function addMailers(array $mails = [])
    {
        $this->mailers = $mails;
    }

    /**
     * Configure and send all emails
     */
    public function send()
    {
        $this->getSendData();
        $mode = $this->mode . 'Output';
        $this->$mode();
    }

    /**
     * Creates mailing list, based on previously configured mailer as customers list
     *
     * @return array - Configuration for mailing
     */
    protected function getSendData()
    {
        foreach ($this->mailers as $mailer)
            if ($mailer instanceof Email)
                foreach ($this->customers as $customer)
                    if ($mailer->shouldSend($customer))
                        $this->mailList[get_class($mailer)][] = $mailer->createMail($customer);

        return $this->mailList;
    }

    /**
     * Used for output in "debug" mode, emulates sending.
     */
    protected function debugOutput()
    {
        $date = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        echo "MODE: debug\r\nTask start " . $date->format("m.d.Y H:i:s.u") . "\r\n";

        foreach ($this->mailList as $mailType => $mails) {
            foreach ($mails as $mail) {
                echo "Send {$mailType} to {$mail['receiver']}\r\n";
            }
        }

        $date = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        echo "Send " . array_sum(array_map(function ($el) { return count($el); }, $this->mailList)) . " mails\r\nTask complete " . $date->format("m.d.Y H:i:s.u") . "\r\n";
    }

    /**
     * Used for output in "production" mode. Send mails by using @function mail()
     */
    protected function productionOutput()
    {
        $succeed = 0;
        $failed = 0;

        $date = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        echo "MODE: production\r\nTask start " . $date->format("m.d.Y H:i:s.u") . "\r\n";

        foreach ($this->mailList as $mailType => $mails) {
            foreach ($mails as $mail) {
                try {
                    echo "Send {$mailType} to {$mail['receiver']}\r\n";

                    if (mail('r.kapatila@gmail.com', $mail['subject'], $mail['message'], $mail['headers'])) {
                        echo "Success!\r\n";
                        $succeed++;
                    } else {
                        echo "Failed!\r\n";
                        $failed++;
                    }
                } catch (Exception $exception) {
                    echo $exception;
                }
            }
        }

        $date = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));

        echo "Send " . array_sum(array_map(function ($el) { return count($el); }, $this->mailList)) . " mails\r\n";
        echo "Success: \t{$succeed}\tFail: \t{$failed}\r\n";
        echo "Task complete " . $date->format("m.d.Y H:i:s.u") . "\r\n";
    }
}