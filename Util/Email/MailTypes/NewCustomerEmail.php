<?php

class NewCustomerEmail extends Email
{
    public function getMessage(Customer $customer)
    {
        return "Hi {$customer->email}<br>We would like to welcome you as customer on our site!<br><br>Best Regards,<br>Forbytes Team";
    }

    public function getSubject(Customer $customer)
    {
        return "Welcome as a new customer";
    }

    public function shouldSend(Customer $customer = null)
    {
        return $customer->createdAt > (new DateTime())->modify('-1 day');
    }
}