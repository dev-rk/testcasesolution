<?php

class RemindEmail extends Email
{
    protected $token = '';

    public function __construct($sender = '', $token = '')
    {
        parent::__construct($sender);

        $this->setToken($token);
    }

    public function setToken($token = '')
    {
        $this->token = $token;
    }

    public function getMessage(Customer $customer = null)
    {
        return "Hi {$customer->email}<br>We miss you as a customer. Our shop is filled with nice products. Here is a voucher that gives you 50 kr to shop for.<br>Voucher: {$this->token}<br><br>Best Regards,<br>Forbytes Team";
    }

    public function getSubject(Customer $customer = null)
    {
        return "We miss you as a customer";
    }

    public function shouldSend(Customer $customer = null)
    {
        return ! isset($customer->orders) || empty($customer->orders);
    }
}