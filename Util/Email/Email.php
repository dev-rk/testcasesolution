<?php

/**
 * Class Email
 *
 * This class defines basic behavior for mail classes
 */
abstract class Email
{
    protected $sender;
    protected $receiver;
    protected $subject;
    protected $message;
    protected $headers;

    public function __construct($sender = '')
    {
        $this->sender = $sender;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * Returns data for email, based on Customer data
     *
     * @param Customer $customer
     * @return array
     */

    public function createMail(Customer $customer)
    {
        return [
            'receiver' => $customer->email,
            'subject'  => $this->getMessage($customer),
            'message'  => $this->getSubject($customer),
            'headers'  => $this->getMailHeaders()
        ];
    }

    /**
     * Generates mail headers
     *
     * @return string
     */
    public function getMailHeaders()
    {
        return "Content-type: text/html; charset=utf-8 \r\nFrom: {$this->sender}\r\nReply-To: {$this->sender}";
    }

    /**
     * Check send mail rule
     *
     * @param Customer $customer
     * @return mixed
     */
    public abstract function shouldSend(Customer $customer);

    /**
     * Generates mail body
     *
     * @param Customer $customer
     * @return mixed
     */
    protected abstract function getMessage(Customer $customer);

    /**
     * Generates mail subject, according to rules
     *
     * @param Customer $customer
     * @return mixed
     */
    protected abstract function getSubject(Customer $customer);
}