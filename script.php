<?php
require_once "require.php";

$customers = DataLayer::ListCustomers();
$orders = DataLayer::ListOrders();

$mailer = new Mailer('debug');

$mailer->addCustomers($customers, $orders);
$mailer->addMailers([
    new NewCustomerEmail("infs@forbytes.com"),
    new RemindEmail("infor@forbytes.com", "ComebackToUs")
]);

$mailer->send();

/**
 * To improve basic functionality:
 * 1. Put mail generating rules as separate abstract class, called Email;
 * 2. Create needed child classes (NewCustomerEmail, RemindEmail)
 * 3, Create class, that should perform managing all emails.
 *
 * Notice: I think that created functionality can be easy extended with new mail types, also it can be easy to add new output.
 * Main disadvantages is higher system resource requirements and execution time - objects needs more memory, so we got a larger execution time.
 */